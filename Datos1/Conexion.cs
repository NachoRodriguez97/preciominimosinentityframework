﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class Conexion
    {
        private SqlConnection _conexion = new SqlConnection("Server = DESKTOP-D8LF4H9\\SQLEXPRESS; Database = tienda; Integrated Security = true");
        
        public SqlConnection AbrirConexion()
        {
            if(_conexion.State == ConnectionState.Closed)
            {
                _conexion.Open();
            }
            return _conexion;
        }

        public SqlConnection CerrarConexion()
        {
            if(_conexion.State == ConnectionState.Open)
            {
                _conexion.Close();
            }
            return _conexion;
        }
    }
}
