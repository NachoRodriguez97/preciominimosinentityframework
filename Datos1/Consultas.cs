﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using Datos;

namespace Datos
{
    public class Consultas
    {
        private Conexion conexion = new Conexion();

        SqlDataReader leer;
        DataTable tabla = new DataTable();
        SqlCommand comando = new SqlCommand();

        public DataTable MostrarTablas()
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "MostrarProductos";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        public DataTable MostrarResultadoWinform()
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "MostrarResultado_Winform";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;

        }

        public DataTable PrecioMinimo(double precio)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "PrecioMinimo";
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@precioMinimo", precio);
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        public void Insertar(int cantRegistros)
        {
            DateTime fecha = DateTime.Now;
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "InsertarEnReultado_Winform";
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@fecha", fecha);
            comando.Parameters.AddWithValue("@cantRegistros", cantRegistros);
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }
    }
}
