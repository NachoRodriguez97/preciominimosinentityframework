﻿using System;
using System.Windows.Forms;
using Negocio;

namespace Presentacion
{
    public partial class BtnBuscar : Form
    {
        private Productos productos = new Productos();

        public BtnBuscar()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MostarProductos();
        }

        private void MostarProductos()
        {
            Productos producto = new Productos();
            dataGridView1.DataSource = producto.MostrarProducto();
        }

        //Busador precio > precio minimo
        private void button1_Click(object sender, EventArgs e)
        {
            Productos precioMinimo = new Productos();
            try
            {
               dataGridView1.DataSource = precioMinimo.PrecioMinimo(txtPrecioMinimo.Text);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error" + ex);
            }
        }

        // Guarda resultados de la busqueda en una nueva tabla
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                productos.Insertar(txtPrecioMinimo.Text);
                MessageBox.Show("Los registros fueron insertados correctamente");
                LimpiarForm();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error" + ex);
            }
       
        }

        // Muestra la tabla con los resultados de las busquedas
        private void button3_Click(object sender, EventArgs e)
        {
            Productos producto = new Productos();
            dataGridView1.DataSource = producto.MostrarResultadoWinform();
        }

        private void LimpiarForm()
        {
            txtPrecioMinimo.Clear();
        }
    }
}
