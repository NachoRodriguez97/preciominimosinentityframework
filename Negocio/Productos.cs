﻿using System;
using System.Data;
using Datos;

namespace Negocio
{
    public class Productos
    {
        private Consultas productos = new Consultas();

        public DataTable MostrarProducto()
        {
            DataTable tabla = new DataTable();
            tabla = productos.MostrarTablas();
            return tabla;
        }

        public DataTable MostrarResultadoWinform()
        {
            DataTable tabla = new DataTable();
            tabla = productos.MostrarResultadoWinform();
            return tabla;
        }

        public DataTable PrecioMinimo(string precioMinimo)
        {
            DataTable tabla = new DataTable();
            tabla = productos.PrecioMinimo(Convert.ToDouble(precioMinimo));
            return tabla;
        }

        public void Insertar(string cantRegistros)
        {
            productos.Insertar(Convert.ToInt32(cantRegistros));
        }
    }
}
